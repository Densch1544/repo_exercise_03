﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class SampleEntity : MonoBehaviour
    {
        [SerializeField]
        private string pName = "Dennis";         //Name des Spielers
        [SerializeField]
        private int health = 100;               //Leben des Spielers
        [SerializeField]
        private int mana = 200;                 //Mana des Spielers
        [SerializeField]
        private bool isPlayerCursed = false;    //Moeglicher Status des Spielers
        [SerializeField]
        private float speed = 10.5f;            //LAufgeschwindigkeit des Spielers

        public string PName
        {
            set { pName = value; }
            get { return pName; }
        }


        public int Health
        {
            set { health = value; }
            get { return health; }
        }

        public int Mana
        {
            set { mana = value; }
            get { return mana; }
        }

        public bool IsPlayerCursed
        {
            set { isPlayerCursed = value; }
            get { return isPlayerCursed; }
        }

        public float Speed
        {
            set { speed = value; }
            get { return speed; }
        }

        public void Curse()     //Spiler mit dem Status Curse belegt
        {

        }

        public void Buff(int buffed)    //Spieler buffen
        {

        }







    }
}

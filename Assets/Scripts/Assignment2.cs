﻿using UnityEngine;
using System.Collections;

namespace CoAHomework
{
    public class Assignment2 : MonoBehaviour
    {
        [SerializeField]
        private int attackDamage = 13;      //Angriffstaerke des Gegners
        [SerializeField]
        private float attackSpeed = 1.8f;   //Angriffgeschwindigkeit des Gegners

        private bool isEnemyCursed = true;  // moeglicher Status des Gegners
        private string enemyName = "InnerDemon"; //Name des Gegners

        public int AttackDamage
        {
            get { return attackDamage; }
        }
        public float AttackSpeed
        {
            get { return attackSpeed; }
        }

        public bool IsEnemyCursed
        {
            set { isEnemyCursed = value; }
        }
        public string EnemyName
        {
            set { enemyName = value; }
        }
    }
}
